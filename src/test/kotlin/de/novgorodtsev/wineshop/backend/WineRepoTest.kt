package de.novgorodtsev.wineshop.backend

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import de.novgorodtsev.wineshop.backend.repo.Wine
import de.novgorodtsev.wineshop.backend.repo.WineRepo
import org.junit.After
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.fail

class WineRepoTest {
    private val wineJson: String
        get() = """{"points": "87", "title": "Nicosia 2013 Vulk\u00e0 Bianco  (Etna)", "description": "Aromas include tropical fruit, broom, brimstone and dried herb. The palate isn't overly expressive, offering unripened apple, citrus and dried sage alongside brisk acidity.", "taster_name": "Kerin O\u2019Keefe", "taster_twitter_handle": "@kerinokeefe", "price": null, "designation": "Vulk\u00e0 Bianco", "variety": "White Blend", "region_1": "Etna", "region_2": null, "province": "Sicily & Sardinia", "country": "Italy", "winery": "Nicosia"}""".trimMargin()

    val w = jacksonObjectMapper().readValue(wineJson, Wine::class.java)

    @After
    fun clear() = WineRepo.clear()

    @Test
    fun getTest() {
        assertSize(0)
        val added = WineRepo.add(w)
        assertSize(1)
        assertEquals(w, WineRepo.get(added.id ?: fail()))
        assertEquals(w, WineRepo.get(added.id?.toString() ?: fail()))
    }

    @Test(expected = IllegalArgumentException::class)
    fun getNonExistingTest() {
        WineRepo.get(1)
    }

    @Test(expected = IllegalArgumentException::class)
    fun getNonExistingByStringTest() {
        WineRepo.get("1")
    }

    /**
     * Saving the same object twice will have no effect
     */
    @Test
    fun saveTest() {
        assertSize(0)
        val add1 = WineRepo.add(w)
        assertSize(1)
        val add2 = WineRepo.add(w)
        assertSize(1)
        assertEquals(add1, add2)
    }

    @Test
    fun deleteTest() {
        val added = WineRepo.add(w)
        assertSize(1)
        WineRepo.remove(added.id ?: fail())
        assertSize(0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun deleteNonExistingTest() {
        WineRepo.remove(1)
    }

    @Test(expected = IllegalArgumentException::class)
    fun deleteNonExistingByStringTest() {
        WineRepo.remove("1")
    }

    @Test
    fun deleteByObjectTest() {
        WineRepo.add(w)
        assertSize(1)
        WineRepo.remove(w)
        assertSize(0)
    }

    @Test(expected = IllegalArgumentException::class)
    fun deleteNonExistingByObjectTest() {
        WineRepo.remove(w)
    }

    private fun assertSize(int: Int) {
        assertEquals(int, WineRepo.getAll().size)
    }

}