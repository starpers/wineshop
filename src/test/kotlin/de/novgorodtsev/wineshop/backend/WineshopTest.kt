package de.novgorodtsev.wineshop.backend

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import de.novgorodtsev.wineshop.backend.repo.Wine
import de.novgorodtsev.wineshop.backend.repo.WineRepo
import io.ktor.application.Application
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.server.testing.TestApplicationEngine
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

class WineshopTest {

    private val json = "application/json"
    private val wineJson: String
        get() = """{"points": "87", "title": "Nicosia 2013 Vulk\u00e0 Bianco  (Etna)", "description": "Aromas include tropical fruit, broom, brimstone and dried herb. The palate isn't overly expressive, offering unripened apple, citrus and dried sage alongside brisk acidity.", "taster_name": "Kerin O\u2019Keefe", "taster_twitter_handle": "@kerinokeefe", "price": null, "designation": "Vulk\u00e0 Bianco", "variety": "White Blend", "region_1": "Etna", "region_2": null, "province": "Sicily & Sardinia", "country": "Italy", "winery": "Nicosia"}""".trimMargin()
    private val wineJson2: String
        get() = """{"points": "87", "title": "Nicosia 2014 Vulk\u00e0 Bianco  (Etna)", "description": "Aromas include tropical fruit, broom, brimstone and dried herb. The palate isn't overly expressive, offering unripened apple, citrus and dried sage alongside brisk acidity.", "taster_name": "Kerin O\u2019Keefe", "taster_twitter_handle": "@kerinokeefe", "price": null, "designation": "Vulk\u00e0 Bianco", "variety": "White Blend", "region_1": "Etna", "region_2": null, "province": "Sicily & Sardinia", "country": "Italy", "winery": "Nicosia"}""".trimMargin()
    private val fileName = "winemag-data-130k-v2.json.gz"

    @After
    fun clear() = WineRepo.clear()


    @Test
    fun firstGetTest() = withTestApplication(Application::main) {
        with(handleRequest(HttpMethod.Get, "/")) {
            assertEquals(HttpStatusCode.OK, response.status())
        }
    }

    @Test
    fun getAllWinesTest() = withTestApplication(Application::main) {
        val wine = saveWine(wineJson)
        val wine2 = saveWine(wineJson2)
        handleRequest(HttpMethod.Get, REST_ENDPOINT) {
            addHeader("Accept", json)
        }.response.let { it ->
            assertEquals(HttpStatusCode.OK, it.status())
            val response = jacksonObjectMapper().readValue(it.content, Array<Wine>::class.java)
            response.forEach { println(it) }
            response.find { it.title == wine.title } ?: fail()
            response.find { it.title == wine2.title } ?: fail()
        }
        assertTrue(2 == WineRepo.getAll().size)
    }


    @Test
    fun saveWineTest() = withTestApplication(Application::main) {
        val initSize = WineRepo.getAll().size
        val wine = saveWine()
        assertEquals(HttpStatusCode.OK, handleRequest(HttpMethod.Get, "$REST_ENDPOINT/${wine.id}") {
        addHeader("Accept", json)
    }.response.status())
        assertEquals(initSize + 1, WineRepo.getAll().size)
    }

    @Test
    fun deleteWineTest() = withTestApplication(Application::main) {
        val initSize = WineRepo.getAll().size
        val wine = saveWine()
        assertEquals(initSize + 1, WineRepo.getAll().size)
        assertEquals(HttpStatusCode.OK, handleRequest(HttpMethod.Delete, "$REST_ENDPOINT/${wine.id}") {
        addHeader("Accept", json)
    }.response.status())
        assertEquals(initSize, WineRepo.getAll().size)
    }

    @Test
    fun loadWinesFromJsonZipTest() = withTestApplication(Application::main) {
        assertEquals(HttpStatusCode.OK, handleRequest(HttpMethod.Get, "$REST_ENDPOINT/addFromJsonZipFile/$fileName") {
        addHeader("Accept", json)
    }.response.status())
        assertEquals(129971, WineRepo.getAll().size)
    }


    private fun TestApplicationEngine.saveWine(wine: String = wineJson): Wine {
        val post = handleRequest(HttpMethod.Post, REST_ENDPOINT) {
            body = wine
            addHeader("Content-Type", json)
            addHeader("Accept", json)
        }
        with(post) {
            assertEquals(HttpStatusCode.OK, response.status())
            println(response.content)
            return jacksonObjectMapper().readValue(response.content, Wine::class.java)
        }
    }


}