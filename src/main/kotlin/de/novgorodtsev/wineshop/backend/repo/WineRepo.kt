package de.novgorodtsev.wineshop.backend.repo

import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import java.io.InputStream
import java.util.concurrent.atomic.AtomicInteger
import java.util.zip.GZIPInputStream
import kotlin.system.measureTimeMillis


object WineRepo {

    private val idCounter = AtomicInteger()
    private val wines: MutableList<Wine> = ArrayList()

    fun add(w: Wine): Wine {
        if (wines.contains(w)) {
            return wines.find { it == w }!!
        }
        w.id = idCounter.incrementAndGet()
        wines.add(w)
        return w
    }

    fun addFromJsonFile(jsonFileName: String): String {
        val url = WineRepo::class.java.getResource("/$jsonFileName")
        return loadWines(url.openStream())
    }

    fun addFromJsonZipFile(jsonZipFileName: String): String {
        val url = WineRepo::class.java.getResource("/$jsonZipFileName")
        return loadWines(GZIPInputStream(url.openStream()))
    }

    private fun loadWines(inputStream: InputStream): String {
        val time = measureTimeMillis {
            val jp = jacksonObjectMapper().factory.createParser(inputStream)
            while (jp.nextToken()!=null) {
                if (jp.currentToken() == JsonToken.START_OBJECT) {
                    val w = jp.readValueAs(Wine::class.java)
                    w.id = idCounter.incrementAndGet()
                    wines.add(w)
                    println("${w.id}: ${w.title}")
                }
            }
        }
        return "Loading is in $time ms done. Actual wines list length = $idCounter."
    }

    fun get(id: String) = wines.find { it.id.toString() == id }
            ?: throw IllegalArgumentException("No entity found for $id")

    fun get(id: Int) = get(id.toString())

    fun getAll() = wines.toList()

    fun remove(w: Wine) {
        if (!wines.contains(w)) {
            throw IllegalArgumentException("Wine isn't exists in repo.")
        }
        wines.remove(w)
    }

    fun remove(id: String) = wines.remove(get(id))

    fun remove(id: Int) = wines.remove(get(id))

    fun clear() = wines.clear()

}