package de.novgorodtsev.wineshop.backend.repo

data class Wine( val country: String?, val description: String?, val designation: String?,
                val points: Int?, val price: Double?, val province: String?, val region_1: String?,
                val region_2: String?,val taster_name: String?, val taster_twitter_handle: String?,
                val title: String?, val variety: String?, val winery: String?){
    var id: Int? = null
}