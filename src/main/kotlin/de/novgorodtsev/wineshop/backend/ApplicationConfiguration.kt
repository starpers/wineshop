package de.novgorodtsev.wineshop.backend

import com.fasterxml.jackson.databind.SerializationFeature
import de.novgorodtsev.wineshop.backend.repo.Wine
import de.novgorodtsev.wineshop.backend.repo.WineRepo
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.html.respondHtml
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.jackson.jackson
import io.ktor.pipeline.PipelineContext
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.delete
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import kotlinx.html.*
import java.text.DateFormat
import java.time.Duration

const val REST_ENDPOINT = "/wines"


fun Application.main() {
    install(DefaultHeaders)
    install(CORS) {
        maxAge = Duration.ofDays(1)
    }
    install(ContentNegotiation){
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
            dateFormat = DateFormat.getDateInstance()
            disableDefaultTyping()
        }
    }

    routing {

        get("$REST_ENDPOINT/{id}") {
            errorAware {
                val id = call.parameters["id"] ?: throw IllegalArgumentException("Parameter id not found")
                LOG.debug("Get Winen entity with Id=$id")
                call.respond(WineRepo.get(id))
            }
        }
        get(REST_ENDPOINT) {
            errorAware {
                LOG.debug("Get all Wine entities")
                call.respond(WineRepo.getAll())
            }
        }
        delete("$REST_ENDPOINT/{id}") {
            errorAware {
                val id = call.parameters["id"] ?: throw IllegalArgumentException("Parameter id not found")
                LOG.debug("Delete Wine entity with Id=$id")
                call.respondSuccessJson(WineRepo.remove(id))
            }
        }
        delete(REST_ENDPOINT) {
            errorAware {
                LOG.debug("Delete all Wine entities")
                WineRepo.clear()
                call.respondSuccessJson()
            }        }
        post(REST_ENDPOINT) {
            errorAware {
                val receive = call.receive<Wine>()
                println("Received Post Request: $receive")
                call.respond(WineRepo.add(receive))
            }
        }
        get("$REST_ENDPOINT/addFromJsonFile/{fileName}") {
            errorAware {
                val fileName = call.parameters["fileName"] ?: throw IllegalArgumentException("Parameter fileName not found")
                LOG.debug("Get fileName=$fileName")
                call.respond(WineRepo.addFromJsonFile(fileName))
            }
        }
        get("$REST_ENDPOINT/addFromJsonZipFile/{fileName}") {
            errorAware {
                val fileName = call.parameters["fileName"] ?: throw IllegalArgumentException("Parameter fileName not found")
                LOG.debug("Get fileName=$fileName")
                call.respond(WineRepo.addFromJsonZipFile(fileName))
            }
        }

        get("/") {
            call.respondHtml {
                head {
                    title("ktor Example Application")
                }
                body {
                    h1 { +"Hello DZone Readers" }
                    p {
                        +"How are you doing?"
                    }
                }
            }
        }
    }
}

private suspend fun <R> PipelineContext<*, ApplicationCall>.errorAware(block: suspend () -> R): R? {
    return try {
        block()
    } catch (e: Exception) {
        call.respondText("""{"error":"$e"}""", ContentType.parse("application/json"), HttpStatusCode.InternalServerError)
        null
    }
}

private suspend fun ApplicationCall.respondSuccessJson(value: Boolean = true) = respond("""{"success": "$value"}""")

